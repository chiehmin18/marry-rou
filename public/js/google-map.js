
var google;

function init() {
    var myLatlng = new google.maps.LatLng(25.068251,121.525589);

    var mapOptions = {
        zoom: 14,
        center: myLatlng,
        // styles: [{
        //     "featureType": "poi",
        //     "stylers": [
        //         { "visibility": "off" }
        //     ]
        // }]
    };

    var mapElement = document.getElementById('map');

    var map = new google.maps.Map(mapElement, mapOptions);

	new google.maps.Marker({
		position: myLatlng,
		map: map,
		icon: 'img/loc.png'
	});
}
google.maps.event.addDomListener(window, 'load', init);
